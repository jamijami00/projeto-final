<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<div class="produto-big-container">
		<?php
		$img = wp_get_attachment_image( $product->get_image_id(), 'large' ); 
		echo "<img class='produto-foto' {$img} </img>"; ?>
		<div class="produto-container">
			<?php the_title( '<h1 class="produto-titulo">', '</h1>' ); ?>
			<p class="produto-descricao"><?php echo $product->get_description();?></p>
			<div class="summary entry-summary">
				<?php
				/**
				 * Hook: woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				do_action( 'woocommerce_single_product_summary' );
				?>
				<p id="produto-preco"><?php echo $product->get_price_html(); ?></p>
			</div>
		</div>
	</div>
    
	<!-- Gerar lista de produtos relacionados -->

    <?php         
        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 4,
            'columns'        => 4,
    		'orderby'        => 'rand',
    		'order'          => 'desc',
         );
                    
        $args['related_products'] = array_filter( array_map( 'wc_get_product', wc_get_related_products( $product->get_id(), $args['posts_per_page'], $product->get_upsell_ids() ) ), 'wc_products_array_filter_visible' );
		$args['related_products'] = wc_products_array_orderby( $args['related_products'], $args['orderby'], $args['order'] );

		// Set global loop values.
		wc_set_loop_prop( 'name', 'related' );
		wc_set_loop_prop( 'columns', $args['columns'] );

		wc_get_template( 'single-product/related.php', $args );?>    
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>