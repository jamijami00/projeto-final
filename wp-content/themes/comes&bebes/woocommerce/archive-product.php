<?php
// Template Name: Lista de Produtos
?>
<?php get_header();?>
    <main>
        <section class="page-content text-left">
            <h2 class='no-margin'>SELECIONE UMA CATEGORIA</h2>
            <div class="slider-categorias">
                <?php
$taxonomy = 'product_cat';
$orderby = 'name';
$show_count = 0; // 1 for yes, 0 for no
$pad_counts = 0; // 1 for yes, 0 for no
$hierarchical = 1; // 1 for yes, 0 for no
$title = '';
$empty = 1;

$args = array(
    'taxonomy' => $taxonomy,
    'orderby' => $orderby,
    'show_count' => $show_count,
    'pad_counts' => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li' => $title,
    'hide_empty' => $empty,
);

$categories = get_categories($args);
if ($categories) {
    foreach ($categories as $category) {
        $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
        $image = wp_get_attachment_url($thumbnail_id);
        echo "<figure class='card-categoria'>
                           <a href='" . get_term_link($category->slug, 'product_cat') . "'><img class='card-image' src='{$image}'></a>
                           <p class='card-name'>{$category->name}</p>
                           </figure>";
    }
}
?>
            </div>
        </section>
        <section class="page-content text-left">
            <h2 class='no-margin'>PRATOS</h2>
            <?php
                if (woocommerce_product_loop()) {

                    do_action('woocommerce_before_shop_loop');

                    woocommerce_product_loop_start();

                    if (wc_get_loop_prop('total')) {
                        while (have_posts()) {
                            the_post();
                            do_action('woocommerce_shop_loop');
                            wc_get_template_part('content', 'product');
                        }
                    }
                    woocommerce_product_loop_end();
                    do_action('woocommerce_after_shop_loop');
                } else {
                    do_action('woocommerce_no_products_found');
                }?>
        </section>
    </main>
<?php

get_footer();