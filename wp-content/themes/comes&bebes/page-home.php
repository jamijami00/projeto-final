<?php
// Template Name: Pagina Inicial
?>

<?php get_header(); ?>
    <main>

        <section class="purple-block">
            <h1>Comes&Bebes</h1>
            <sub>O restaurante para todas as fomes</sub>
        </section>

        <section class="page-content">
            <h2>CONHEÇA NOSSA LOJA</h2>
            <h3>Tipos de pratos principais</h3>
            <div class="slider-categorias">
                <?php
                   $taxonomy     = 'product_cat';
                   $orderby      = 'name';  
                   $show_count   = 0;      // 1 for yes, 0 for no
                   $pad_counts   = 0;      // 1 for yes, 0 for no
                   $hierarchical = 1;      // 1 for yes, 0 for no  
                   $title        = '';  
                   $empty        = 1;
                 
                   $args = array(
                          'taxonomy'     => $taxonomy,
                          'orderby'      => $orderby,
                          'show_count'   => $show_count,
                          'pad_counts'   => $pad_counts,
                          'hierarchical' => $hierarchical,
                          'title_li'     => $title,
                          'hide_empty'   => $empty
                   );

                   $categories = get_categories($args);
                   if($categories){
                       foreach($categories as $category){
                           $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
                           $image = wp_get_attachment_url($thumbnail_id);
                           echo "<figure class='card-categoria'>
                           <a href='". get_term_link( $category->slug, 'product_cat' ) . "'><img class='card-image' src='{$image}'></a>
                           <p class='card-name'>{$category->name}</p>
                           </figure>";
                       }
                   } 
                ?>
            </div>

            <h3>Pratos do dia de hoje:</h3>
            <div class="dia-semana">
                <?php   
                    setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                    date_default_timezone_set('America/Sao_Paulo');
                    $day=strftime(ucwords("%w",time()));
                    switch ($day) {
                        case 0:
                            echo "DOMINGO";
                            break;
                        case 1:
                            echo "SEGUNDA";
                            break;
                        case 2:
                            echo "TERÇA";
                            break;
                        case 3:
                            echo "QUARTA";
                            break;
                        case 4:
                            echo "QUINTA";
                            break;
                        case 5:
                            echo "SEXTA";
                            break;
                        case 6:
                            echo "SÁBADO";
                            break;  
                    }
                ?>
            </div>
            <div class="slider-promocao">
                <?php         
                        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');
                        $day=strftime(ucwords("%a",time()));

                        $args = array(
                            'post_type'      => 'product',
                            'posts_per_page' => 4,
                            'product_tag'    => $day,
                        );
                    
                        $loop = new WP_Query($args);
                    
                        while ($loop->have_posts()) : $loop->the_post();
                            global $product;
                            $imagem = woocommerce_get_product_thumbnail();
                            $titulo = get_the_title();
                            $preco = wc_price($product->get_price_including_tax(1,$product->get_price()));;  
                            echo "<figure class ='card-promocao'>
                            <img {$imagem} </img>
                            <div class='card-promocao-dados'>
                            <p id='card-titulo'>{$titulo}</p>
                            <p id='card-preco'>{$preco}</p> 
                            <a href='". get_permalink( )."'><button class='button-buy'></button></a>
                            </div>
                            </figure>"; 
                        endwhile;
                    
                        wp_reset_query();
                ?>    
            </div>
            <a href="/shop"><button class="box-button-yellow" id="outras-opcoes">Veja outras opções</button></a>
        </section>

        <section class="purple-block">
            <h2>VISITE NOSSA LOJA FÍSICA</h2>
            <div class="about-us">
                <div class="mapa-e-endereco">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3281.707735541587!2d-43.189604468863784!3d-22.594347451788856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1634070265056!5m2!1spt-BR!2sbr" width="345" height="203" style="border:0;" allowfullscreen="no" loading="lazy"></iframe>
                    <div class="endereco">
                        <div><img class ="icon" src="<?php echo get_stylesheet_directory_uri() ?>/img/food-icon.png" alt="endereço" width="19.12" height="18.16">Rua lorem ipsum, 123, LI, Brasil</div>
                        <div><img class ="icon" src="<?php echo get_stylesheet_directory_uri() ?>/img/phone-icon.png" alt="telefone" width="17.73" height="17.75">(XX) XXXX-XXXX</div>
                    </div>
                </div>
                <ul class="slideshow">
                    <li>
                        <input type="radio" id="slide1" name="slidef" checked>
                        <label for="slide1"></label>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>./img/slideshow-img1.jpg" alt="" width=600 height=356>
                    <li>
                        <input type="radio" id="slide2" name="slidef" checked>
                        <label for="slide2"></label>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>./img/slideshow-img2.jpg" alt="" width=600 height=356>
                    <li>
                        <input type="radio" id="slide3" name="slidef" checked>
                        <label for="slide3"></label>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>./img/slideshow-img3.jpg" alt="" width=600 height=356>
                </ul>
            </div>
        </section>

    </main>
<?php get_footer(); ?>