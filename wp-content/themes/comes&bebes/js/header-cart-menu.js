var botao_abrir = document.getElementById("header-cart-icon");
var botao_fechar = document.getElementById("close-cart-bar");

var screen = document.getElementById("darken-screen");
var cart = document.getElementById("cart-bar");

botao_abrir.addEventListener("click", show => {
    screen.classList.toggle("hidden");
    cart.classList.toggle("hidden");
});

botao_fechar.addEventListener("click", show => {
    screen.classList.toggle("hidden");
    cart.classList.toggle("hidden");
});