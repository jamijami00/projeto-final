<?php

// Adiciona suporte ao WooCommerce para meu tema

function mytheme_add_woocommerce_support()
{
    add_theme_support('woocommerce', array(
        'thumbnail_image_width' => 240,
        'single_image_width' => 500,

        'product_grid' => array(
            'default_rows' => 4,
            'min_rows' => 4,
            'max_rows' => 8,
            'default_columns' => 4,
            'min_columns' => 4,
            'max_columns' => 5,
        ),
    ));
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

// Abre/Fecha o sidebar com o Mini Carrinho

function hide_close_cart()
{
    wp_register_script('header-cart-menu', get_template_directory_uri() . '/js/header-cart-menu.js', array('jquery'), '1.0', true);
    wp_enqueue_script('header-cart-menu');
}
add_action('wp_enqueue_scripts', 'hide_close_cart');

// Gera o Mini Carrinho

function cb_carrinho()
{
    $total_value = 0;
    $items = WC()->cart->get_cart();
    foreach ($items as $item => $value) {
        $_product = wc_get_product($value['data']->get_id());
        echo "<div class='produto'>"
        . $_product->get_image() .
        "<div class='produto-info'>
                            <span class='text-left'>" . $_product->get_name() . "</span>
                            <div class='produto-info2'><span>Quantidade: " . $value['quantity'] . "</span>
                            " . wc_price($value['line_subtotal']) . "</div>
                            </div>
                            </div>";
        $total_value += $value['line_subtotal']; //
    }
    if ($total_value != 0) {
        echo "<span class='text-left'><hr>Total do Carrinho: " . wc_price($total_value) . "</span>";
    }
};
add_action('cb-cart', 'cb_carrinho');

    // 1. Show plus minus buttons
    
    add_action( 'woocommerce_after_quantity_input_field', 'silva_display_quantity_plus' );
    
    function silva_display_quantity_plus() {
    echo '<button type="button" class="plus" >+</button>';
    }
    
    add_action( 'woocommerce_before_quantity_input_field', 'silva_display_quantity_minus' );
    
    function silva_display_quantity_minus() {
    echo '<button type="button" class="minus" >-</button>';
    }
    
    // -------------
    // 2. Trigger update quantity script
    
    add_action( 'wp_footer', 'silva_add_cart_quantity_plus_minus' );
    
    function silva_add_cart_quantity_plus_minus() {
    
    if ( ! is_product() && ! is_cart() ) return;
        
    wc_enqueue_js( "   
            
        $('form.cart,form.woocommerce-cart-form').on( 'click', 'button.plus, button.minus', function() {
    
            var qty = $( this ).parent( '.quantity' ).find( '.qty' );
            var val = parseFloat(qty.val());
            var max = parseFloat(qty.attr( 'max' ));
            var min = parseFloat(qty.attr( 'min' ));
            var step = parseFloat(qty.attr( 'step' ));
    
            if ( $( this ).is( '.plus' ) ) {
                if ( max && ( max <= val ) ) {
                qty.val( max );
                } else {
                qty.val( val + step );
                }
            } else {
                if ( min && ( min >= val ) ) {
                qty.val( min );
                } else if ( val > 1 ) {
                qty.val( val - step );
                }
            }
    
        });
            
    " );
    }

