<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <title><?php bloginfo('Comes&Bebes')?> <?php wp_title('|'); ?></title>
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="darken-screen hidden" id="darken-screen"></div>
        <div class="cart-bar hidden" id="cart-bar">
            <span id="close-button"><button id="close-cart-bar">X</button></span>
            <h2 class="text-left">CARRINHO<hr></h2>
            <div class="cart-list">
                <?php do_action('cb-cart');?>
            </div>
            <a href="/checkout"><button class="box-button-purple">Comprar</button></a>
        </div>
        <div class="page-header">
            <a href="http://comesbebes.local/"><input type="image" id="header-logo" src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png"></a>
            <div class="header-search-bar">
                <form action="<?php echo bloginfo('url');?>/shop/" method="get">
                    <input type="image" src="<?php echo get_stylesheet_directory_uri() ?>/img/search-icon.png">
                    <input type="text" name="s" id="s" placeholder="Pesquisar..." value="<?php echo the_search_query(  )?>">
                    <input type="text" name="post_type" value="product" class="hidden">
                </form>
            </div>
            <a href="/shop"><button class="box-button-yellow">Faça um Pedido</button></a>
            <input type="image" id="header-cart-icon" src="<?php echo get_stylesheet_directory_uri() ?>/img/cart-icon.png">
            <a href="/my-account"><input type="image" id="header-user-icon" src="<?php echo get_stylesheet_directory_uri() ?>/img/user-icon.png"></a>
        </div>
        <nav class="nav-bar hidden" id="nav-bar">
            <ul class="nav-list">
                <li><a href="/my-account">PAINEL</a></li>
                <li><a href="/my-account/pedidos">PEDIDOS</a></li>
                <li><a href="/my-account/editar-endereco">ENDEREÇOS</li></a>
                <li><a href="/my-account/deslogar-cliente">SAIR</a></li>
            </ul>
        </nav>
    </header>