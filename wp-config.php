<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PGzzzcICR1nBTE93gASeM6kys4CnVB1RXPBiXpdYnF9HnYndgzkmAD5XQ1z7MIDWU1R3EjziMTl+kFPWQVnTsQ==');
define('SECURE_AUTH_KEY',  'Z3bQxZgvOHKleC69phLqiLGsmWrQk6OBvZB2SM9Hufrh7K1YSn9O8GX2fa6bY3CnpLqNC4EdCd+zjObHOQzzHA==');
define('LOGGED_IN_KEY',    'xvZB/TpE4tHREm6Zm+cmgiyzgeHGnTJ8ZUSvGam+Z4DDsiUJUKuOOARLbEW7SEPpM6Tm6WjuVgQp9NS3RALDrg==');
define('NONCE_KEY',        'oWgXC0EJIEvaalG2OBzV6geua5xzhtogdttZAhXmS+DHJGT2K2ke5moZS+mUiW1xz7NWbHp/oj7T0vixldfcxg==');
define('AUTH_SALT',        '2hUrZ11O3km3Chw8r0FXuOIEUqHZnzasnBuCJfnWV5zyHlDvSD8dTVnXNHOOrHFYO8Ni6Y/ViNGMzy4HOxiyCw==');
define('SECURE_AUTH_SALT', 'tSOY7kL1Cy1QNpeltNurNsUG8IC3Ic47WO0nZVJ6JrvrIqOa6BDMOvmdWIEDtp3f7VpAin1YI2bpcd0NsuEZMg==');
define('LOGGED_IN_SALT',   'uihR8yjwHv1rA1fi79pW6TbyiL6urwggLReFjOrVrDcq+LHST50yUyrIq2nh4wynwRbw3v7v1BT1LYWyXl4LnQ==');
define('NONCE_SALT',       'lGMqUjzPJGrish980faZ0i4Vxuyt3B2nwrDN5NnyJwGMVO6pNAsGkKwEIC/xgc2+Qty1Heq2EKCFmYhUUYW6wg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
